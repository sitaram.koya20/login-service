package com.auth.UserLogin.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;

public class JWTUtil {

    public static DecodedJWT decodeJWT(String token) {
        DecodedJWT decodedJWT = null;
        try {
            Algorithm algo = Algorithm.HMAC256("secret");
            JWTVerifier verifier = JWT.require(algo).build();
            decodedJWT = verifier.verify(token);

        }
        catch (Exception e ){

        }
        return decodedJWT;
    }

    public static String getUserName( DecodedJWT decodedJWT) throws Exception {
        return decodedJWT.getSubject();
    }
    public static String getUserName(String token){
        DecodedJWT decodedJWT = decodeJWT(token);
        return decodedJWT.getSubject();
    }

    public static boolean isJWTExpired( DecodedJWT decodedJWT) throws Exception {
        if(decodedJWT.getExpiresAt().after(new Date())) return false;
        return true;
    }
}
