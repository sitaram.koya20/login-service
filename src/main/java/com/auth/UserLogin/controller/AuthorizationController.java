package com.auth.UserLogin.controller;

import com.auth.UserLogin.model.RequestType;
import com.auth.UserLogin.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthorizationController {
    @Autowired
    AuthorizationService authorizationService;

    @PostMapping ("/authorizeRequest")
    public RequestType authorizeRequest(@RequestBody RequestType requestType) throws Exception {
        return authorizationService.authorizeRequest(requestType);
    }
}
