package com.auth.UserLogin.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestType {
    private String token;
    private String methodType;
    private String UrlPath;
    private String loggedInUserName;
    private boolean isAuthorized;
}