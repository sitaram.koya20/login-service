package com.auth.UserLogin.model;

import java.util.Arrays;
import java.util.List;

public enum Method {
    GET("USER","ADMIN"),PUT("ADMIN"),POST("ADMIN"),DELETE("ADMIN");
    List<String> RequiredRoles;

    Method(String ... methodType) {
        this.RequiredRoles = Arrays.asList(methodType);
    }
    public List<String> getRequiredRoles(){
        return this.RequiredRoles;
    }
}
