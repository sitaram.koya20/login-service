package com.auth.UserLogin.client;

import com.auth.UserLogin.model.User;
import io.reactivex.Single;

public interface QuestionAnswerService {
    Single<User> saveUserDetails(User user);
}
