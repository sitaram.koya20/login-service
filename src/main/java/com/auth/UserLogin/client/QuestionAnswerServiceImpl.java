package com.auth.UserLogin.client;

import com.auth.UserLogin.model.User;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class QuestionAnswerServiceImpl implements QuestionAnswerService {

    @Autowired
    RestTemplate restTemplate;

    public final static String URL = "http://localhost:8080/stackoverflow/users/saveUser";


    public User executeRequest(User user) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<User> entity = new HttpEntity<User>(user,headers);
        restTemplate.exchange(URL, HttpMethod.POST,
                entity,
                User.class);
        return user;
    }

    @Override
    public Single<User> saveUserDetails(User user) {
        Single<User> saveUserBSSingle = Single.create(sub ->
                sub.onSuccess(
                        executeRequest(user)
                ));

        return saveUserBSSingle;

    }
}
