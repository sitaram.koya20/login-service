package com.auth.UserLogin;


import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
@EnableSwagger2
@Import(SpringDataRestConfiguration.class)
public class SwaggerConfig {

/*    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.auth.UserLogin"))
                .build().apiInfo(metaData());

    }

    private ApiInfo metaData() {
        return new ApiInfoBuilder()
                .title("Stackoverflow")
                .description("\"Creating Stackoverflow application\"")
                .version("1.1.0")
                .build();
    }
    */

    @Bean
    public Docket apiDocket(){
        return new Docket(SWAGGER_2).apiInfo(apiInfo()).forCodeGeneration(true)
                .securityContexts(singletonList(securityContext()))
                .securitySchemes(singletonList(apiKey())).select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(PathSelectors.regex(SECURE_PATH)).build()
                .tags(new Tag(API_TAG, "All APIs related to Stackoverflow login service"));
    }

   public static final String SECURITY_REFERENCE = "Token Access";
    public static final String AUTHORIZATION_DESCRIPTION= " Full API Permission";
    public static final String AUTHORIZATION_SCOPE = "UNLIMITED";
    public static final String CONTACT_NAME= "STACKOVERFLOW LOGIN API Support";
    /*lic static final String API_TITLE = "Stackoverflow Login open API";
    public static final String API_DESCRIPTION =" Stackoverflow login documentation  " +
            "</br></br></h3> /Please log into your account to access your key <a target='_blank' href=\"http://localhost:7000/stackoverflow/login\">here</a></h3>.";
    public static final String API_VERSION ="1.1.0";
    public static final String LICENSE = "Apache License 2.1.0";
    public static final String LICENSE_URL = "https://www.apache.org/licenses/LICENSE-2.0";*/
    public static final String SECURE_PATH = "/*/.*";
    public static final String API_TAG =" Login Service";


    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("Stackoverflow Login open API")
                .description(" Stackoverflow login documentation  " + "</br></br></h3> Please log into your account to access your key <a target='_blank' href=\"http://localhost:7000/stackoverflow/login\">here</a></h3>.")
                .version("1.1.0")
                .license("Apache License 2.1.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .build();
    }





/*    *//*private ApiInfo apiInfo(){
        return new ApiInfo(API_TITLE, API_DESCRIPTION, API_VERSION, contact(), LICENSE, LICENSE_URL);
    }*//*

    private Contact contact() {
        return new Contact(CONTACT_NAME);*/



    private ApiKey apiKey() {
        return new ApiKey(SECURITY_REFERENCE, AUTHORIZATION, SecurityScheme.In.HEADER.name());

    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(securityReference()).build();
    }

    private List<SecurityReference> securityReference() {
        AuthorizationScope[] authorizationScope ={ new AuthorizationScope(AUTHORIZATION_SCOPE, AUTHORIZATION_DESCRIPTION)};
        return singletonList(new SecurityReference(SECURITY_REFERENCE ,authorizationScope));


        }

}