package com.auth.UserLogin.service;

import com.auth.UserLogin.model.Role;
import com.auth.UserLogin.model.Tag;
import com.auth.UserLogin.model.User;
import com.auth.UserLogin.repository.RoleRepository;
import com.auth.UserLogin.repository.TagRepository;
import com.auth.UserLogin.repository.UserRepository;
import io.reactivex.Single;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    HttpServletRequest request;

    private final UserRepository userRepository;
    private final TagRepository tagRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;


    //This is the method we have to implement for implementing the UserDetailsService
    //In order to provide the bean to the userDetailsService we created in the SecurityConfig
    //we implement it in the UserServiceImpl layer
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepository.findByUserName(userName);
        if(user == null){
            log.error("User not found in the database");
            throw new UsernameNotFoundException("User not found in the database");
        } else{
            log.info("User found in the database");
        }
        /*Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });*/
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getTags().forEach(tag -> {
            authorities.add(new SimpleGrantedAuthority(tag.getTagName()));
        });

        return new org.springframework.security.core.userdetails
                .User(user.getUserName(), user.getPassword(), authorities) ;
    }

    //Regular Service operations
    @Override
    public User saveUser(User user) {
        log.info("Saving user {} to the Database", user.getUserName());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public Role saveRole(Role role) {
        log.info("Saving new role {} to the Database", role.getName());
        return roleRepository.save(role);
    }

    @Override
    public Tag saveTag(Tag tag) {
        log.info("Saving new tag {} to the Database", tag.getTagName());
        return tagRepository.save(tag);
    }


    @Override
    public void addRoleToUser(String userName, String roleName) {
        log.info("Adding role {} to the userName {}", roleName, userName);
        User user = userRepository.findByUserName(userName);
        Role role = roleRepository.findByName(roleName);
        user.getRoles().add(role);
    }

    @Override
    public void addTagToUser(String userName, List<Tag> tagNames) {
        log.info("Adding tag {} to the userName {}");
        User user = userRepository.findByUserName(userName);

        for(Tag _tag: tagNames){
            Tag tag = tagRepository.findByTagName(_tag.getTagName());
            user.getTags().add(tag);
        }
    }

    @Override
    public User getUser(String userName) {
        log.info("Fetching User {}", userName);
        return userRepository.findByUserName(userName);
    }

    @Override
    public List<User> getUsers() {
        log.info("Fetching all Users");
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public User getCurrentUser() {
        org.springframework.security.core.userdetails.User principal =
                (org.springframework.security.core.userdetails.User) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();
        return userRepository.findByUserName(principal.getUsername());
    }

    //rest template
    public Single<User> saveUserDetailsInLS(User user) {
        System.out.println("Saving user in LoginService");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Single<User> userDetailsSingle = Single.create(sub ->
                sub.onSuccess(
                        userRepository.save(user)

                ));

        return userDetailsSingle;
    }

//    @Override
//    public UserPermission saveUserPermission(UserPermission permission) {
//        return userPermissionRepo.save(permission);
//    }

//    @Override
//    public void addPermissionToUser(Long permissionId) {
//        String loggedInUser = (String) request.getAttribute("userName");
//        User user = getUser(loggedInUser);
//        UserPermission userPermission = userPermissionRepo.getById(permissionId);
//        user.get().add(userPermission);
//    }

//    @Override
//    public void addPermissionToUser(String permissionName) {
//        String loggedInUser = (String) request.getAttribute("userName");
//        User user = getUser(loggedInUser);
//        UserPermission userPermission = userPermissionRepo.findByPermissionName(permissionName);
//        user.getUserPermissions().add(userPermission);
//    }
}
