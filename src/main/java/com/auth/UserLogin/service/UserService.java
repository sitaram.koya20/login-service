package com.auth.UserLogin.service;

import com.auth.UserLogin.model.Role;
import com.auth.UserLogin.model.Tag;
import com.auth.UserLogin.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    User saveUser(User user);
    Role saveRole(Role role);
    Tag saveTag(Tag tag);
    void addRoleToUser(String userName, String roleName);
    void addTagToUser(String userName, List<Tag> tagNames);
//    User getCurrentUser();

//    UserPermission saveUserPermission(UserPermission permission);
//
//    void addPermissionToUser(Long permissionId);
//    void addPermissionToUser(String permissionName);

    User getUser(String userName);
    List<User> getUsers();

}
