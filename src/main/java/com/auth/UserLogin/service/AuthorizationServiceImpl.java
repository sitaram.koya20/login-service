package com.auth.UserLogin.service;



import com.auth.UserLogin.model.*;
import com.auth.UserLogin.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorizationServiceImpl implements AuthorizationService{

    @Autowired
    UserService userService;
    @Override
    public RequestType authorizeRequest(RequestType requestType) throws Exception{
            System.out.println(requestType);
        String authHeader = requestType.getToken();
        if(authHeader !=null && authHeader.startsWith("Bearer ")){
            try {
                String userName = JWTUtil.getUserName(authHeader.substring("Bearer ".length()));
                userName = userName.substring(0,userName.length());
                checkAuthorization(requestType,userName);
            }
            catch (Exception e){
                System.out.println("This is a error");
                throw new Exception(e);
            }
        }
        return requestType;
    }

    private void checkAuthorization(RequestType requestType, String userName) throws Exception {
        User user = userService.getUser(userName);
        if(user ==null) throw new Exception("User Not found");
        requestType.setLoggedInUserName(userName);
        List<String> requestedPermission = Method.valueOf(requestType.getMethodType()).getRequiredRoles();
        List<Role> userRoles = user.getRoles();
        //requestType.setAuthorized(true);
        if(!userRoles.isEmpty())
            for(Role role : userRoles){
                if(requestedPermission.contains(role.getName())) {
                    requestType.setAuthorized(true);
                    break;
                }
            }
    }
}
