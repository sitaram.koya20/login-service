package com.auth.UserLogin.service;


import com.auth.UserLogin.model.RequestType;

public interface AuthorizationService {
    RequestType authorizeRequest(RequestType requestType) throws Exception;
}
