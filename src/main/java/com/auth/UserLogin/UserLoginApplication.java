package com.auth.UserLogin;

import com.auth.UserLogin.model.Role;
import com.auth.UserLogin.model.Tag;
import com.auth.UserLogin.model.User;
import com.auth.UserLogin.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
public class UserLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserLoginApplication.class, args);
	}


	//This bean serves for the password encoder we created in the SecurityConfig
	@Bean
	PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}



	//This bean is for populating the data(basically the hard-coded data) which we do in postman.
	@Bean
	CommandLineRunner run(UserService userService) {
		return args -> {
			userService.saveRole(new Role(null, "ADMIN"));
			userService.saveRole(new Role(null, "USER"));

			userService.saveTag(new Tag(null,"C#"));
			userService.saveTag(new Tag(null, "Java"));
			userService.saveTag(new Tag(null, "Python"));


			userService.saveUser(new User(null, "Surya", "surya@gmail.com", "surya123", new ArrayList<>(), new ArrayList<>()));
			userService.saveUser(new User(null, "Sahana", "sahana@gmail.com", "sahana123", new ArrayList<>(), new ArrayList<>()));
			userService.saveUser(new User(null, "Sita", "sita@gmail.com", "sita123", new ArrayList<>(),new ArrayList<>()));

			userService.addRoleToUser("Surya", "ADMIN");
			userService.addRoleToUser("Sahana", "ADMIN");
			userService.addRoleToUser("Sita", "ADMIN");

			userService.addTagToUser("Surya", Arrays.asList(
					new Tag(null,"Java"),
					new Tag(null, "C#")
			));
		};
	}
}
